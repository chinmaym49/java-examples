package com.db.mock;

import com.db.mocking.BankImpl;
import com.db.mocking.IBank;
import com.db.mocking.IBankService;
import com.db.mocking.TransactionType;
import org.junit.*;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class BankingTest {

    IBank bank = null;
    IBankService service = null;

    @BeforeClass
    public static void superInit() {
        System.out.println("Gets called only once, before all the tests");
    }

    @AfterClass
    public static void superDestroy() {
        System.out.println("Gets called only once, after all the tests");
    }

    @Before
    public void init() {
        System.out.println("Gets called before each test");
        service = Mockito.mock(IBankService.class); // this is mocking IBankService
        when(service.transaction(100, "ACC01", TransactionType.DEBIT)).thenReturn("SUCCESS");
        when(service.transaction(100, "ACC01", TransactionType.CREDIT)).thenReturn("SUCCESS");
        bank = new BankImpl(service);
    }

    @After
    public void destroy() {
        System.out.println("Gets called after each test");
    }

    @Test
    public void testWithdraw() {
        String actual = bank.withdraw(100, "ACC01");
        String expected = "SUCCESS";
        assertEquals(expected, actual);
    }

    @Test
    public void testDeposit() {
        String actual = bank.deposit(100, "ACC01");
        String expected = "SUCCESS";
        assertEquals(expected, actual);
    }

}
