package com.db.tests;

import com.db.airportapp.AirportAppFactory;
import com.db.airportapp.IAirportApp;
import org.junit.Test;

import static org.junit.Assert.*;

public class AirportAppTest {

    @Test
    public void testFindByCode() {
        IAirportApp app = AirportAppFactory.getApp();
        assertEquals(app.getAirportByCode("ABC").getName(), "N/A");
        assertEquals(app.getAirportByCode("00FL").getName(), "River Oak Airport");
    }

    @Test
    public void testFIndByName() {
        IAirportApp app = AirportAppFactory.getApp();
        assertEquals(app.getAirportByName("hello world").size(), 0);
    }

    @Test
    public void testFindByCountry() {
        IAirportApp app = AirportAppFactory.getApp();
        assertFalse(app.getAirportsByCountry("US").isEmpty());
    }

    @Test
    public void testFindRandomly() {
        IAirportApp app = AirportAppFactory.getApp();
        assertNotNull(app.getRandomAirport());
    }

    @Test
    public void testFindByElev() {
        IAirportApp app = AirportAppFactory.getApp();
        assertFalse(app.getAirportsByElev(0).isEmpty());
    }

    @Test
    public void testAirportCnt() {
        IAirportApp app = AirportAppFactory.getApp();
        assertEquals(app.getTotalAirports(), 50);
    }

    @Test
    public void testFindByType() {
        IAirportApp app = AirportAppFactory.getApp();
        assertFalse(app.getAirportsByType("heliport").isEmpty());
    }

    @Test
    public void testLogin() {
        IAirportApp app=AirportAppFactory.getApp();
        assert(app.login("abc","123"));
        assertFalse(app.login("def","123"));
    }

    @Test
    public void testRegister() {
        IAirportApp app=AirportAppFactory.getApp();
        assert(app.register("xyz","798"));
        assertFalse(app.register("abc","567"));
    }

    @Test
    public void testInfo() {
        IAirportApp app=AirportAppFactory.getApp();
        assertNotEquals(app.getInfo(),"");
    }


}

