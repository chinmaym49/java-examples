package com.db.lambdas;

import java.nio.file.Files;
import java.nio.file.Paths;

public class LambdaExample {

    public static void main(String[] args) throws Exception {

        Files.readAllLines(Paths.get("E:\\Programming\\Java\\javalearnables\\airport-codes.csv"))
                .stream()
                .skip(1)
                .map((line)->(line.split(",")))
                .filter((row)-> {
                    try {
                        return Integer.parseInt(row[3])>3000;
                    } catch(Exception e) {
                        return false;
                    }
                })
                .forEach((row)-> {
                    System.out.println(row[2]+"\t"+row[7]);
                });
    }
}
