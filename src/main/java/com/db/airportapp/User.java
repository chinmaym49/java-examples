package com.db.airportapp;

public class User {

    private final String handle, password;

    User(String handle, String password) {
        this.handle = handle;
        this.password = password;
    }

    public String getHandle() {
        return handle;
    }

    public String getPassword() {
        return password;
    }


}
