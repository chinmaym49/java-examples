package com.db.airportapp;

public class Airport {

    private final String code, name, country, type;
    private final int elevation;


    Airport(String code, String type, String name, int elevation, String country) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.elevation = elevation;
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public int getElevation() {
        return elevation;
    }

    public String getType() {
        return type;
    }

}
