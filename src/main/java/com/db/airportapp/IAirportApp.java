package com.db.airportapp;

import java.util.List;

public interface IAirportApp {

    public Airport getAirportByCode(String code);
    public List<Airport> getAirportsByCountry(String country);
    public Airport getRandomAirport();
    public List<Airport> getAirportsByElev(int elev);
    public int getTotalAirports();
    public List<Airport> getAirportsByType(String type);
    public List<Airport> getAirportByName(String name);
    public boolean login(String handle,String password);
    public boolean register(String handle,String password);
    public String getInfo();
}
