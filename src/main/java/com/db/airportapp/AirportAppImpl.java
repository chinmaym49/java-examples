package com.db.airportapp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

public class AirportAppImpl implements IAirportApp {

    private List<User> users = null;
    private List<Airport> airports = null;
    private final Random rd = new Random();
    private final String info="This is a boring site all about airports...";

    AirportAppImpl() {

        users = new ArrayList<>();
        users.add(new User("abc", "123"));
        users.add(new User("def", "456"));

        try {
            airports = Files.readAllLines(Paths.get("E:\\Programming\\Java\\javalearnables\\airport-codes.csv"))
                    .stream()
                    .skip(1)
                    .limit(50)
                    .map((row) -> (row.split(",")))
                    .map((row) -> (new Airport(row[0], row[1], row[2], Integer.parseInt(row[3]), row[5])))
                    .collect(Collectors.toList());
        } catch(Exception e) {
            System.out.println("An error occured!");
        }
    }

    @Override
    public Airport getAirportByCode(String code) {
        try {
            return airports.stream()
                    .filter((airport) -> (code.equals(airport.getCode())))
                    .collect(Collectors.toList())
                    .get(0);
        } catch(Exception e) {
            return new Airport("", "", "N/A", -1, "");
        }
    }

    @Override
    public List<Airport> getAirportByName(String name) {
        return airports.stream()
                .filter((airport) -> (name.equalsIgnoreCase(airport.getName())))
                .collect(Collectors.toList());
    }

    @Override
    public List<Airport> getAirportsByCountry(String country) {
        return airports.stream()
                .filter((airport) -> (country.equals(airport.getCountry())))
                .collect(Collectors.toList());
    }

    @Override
    public Airport getRandomAirport() {
        return airports.get(abs(rd.nextInt()) % airports.size());
    }

    @Override
    public List<Airport> getAirportsByElev(int elev) {
        return airports.stream()
                .filter((airport) -> (airport.getElevation() > elev))
                .collect(Collectors.toList());
    }

    @Override
    public int getTotalAirports() {
        return airports.size();
    }

    @Override
    public List<Airport> getAirportsByType(String type) {
        return airports.stream()
                .filter((airport) -> (type.equals(airport.getType())))
                .collect(Collectors.toList());
    }

    @Override
    public boolean login(String handle, String password) {
        int found = (int)users.stream()
                .filter((user) -> (user.getHandle().equals(handle) && user.getPassword().equals(password)))
                .count();
        if(found == 0) {
            System.err.println("Invalid credentials!");
            return false;
        }
        System.out.println("Login successful!");
        return true;
    }

    @Override
    public boolean register(String handle, String password) {
        int found = (int)users.stream()
                .filter((user) -> (user.getHandle().equals(handle)))
                .count();
        if(found == 0) {
            System.out.println("Registration successful!");
            users.add(new User(handle, password));
            return true;
        }
        System.err.println("Username already exists!");
        return false;
    }

    @Override
    public String getInfo() {
        System.out.println(info);
        return info;
    }

}
