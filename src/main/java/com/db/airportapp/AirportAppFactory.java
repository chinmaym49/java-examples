package com.db.airportapp;

public class AirportAppFactory {

    private static IAirportApp app = new AirportAppImpl();

    private AirportAppFactory() {}

    public static IAirportApp getApp() {
        return app;
    }

}
