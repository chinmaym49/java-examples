package com.db.mocking;

/**
 * @author me
 * CUT Class Under Testing
 */
public class BankImpl implements IBank {

    /**
     * Dependency (Not implemented)
     */
    private IBankService service = null;

    public BankImpl(IBankService service) {
        this.service = service;
    }

    /**
     * MUT Method Under Testing
     * @param amt In INR
     * @param id Acc
     * @return Success/Failure
     */
    @Override
    public String withdraw(int amt, String id) {
        if(amt <= 0) {
            throw new IllegalArgumentException("Amt should be more than 0!");
        }
        if(id.length()==0) {
            throw new IllegalArgumentException("ID shouldn't be left blank!");
        }
        return service.transaction(amt, id, TransactionType.DEBIT);
    }

    /**
     * MUT Method Under Testing
     * @param amt In INR
     * @param id Acc
     * @return Success/Failure
     */
    @Override
    public String deposit(int amt, String id) {
        if(amt <= 0) {
            throw new IllegalArgumentException("Amt should be more than 0!");
        }
        if(id.length()==0) {
            throw new IllegalArgumentException("ID shouldn't be left blank!");
        }
        return service.transaction(amt, id, TransactionType.CREDIT);
    }
}
