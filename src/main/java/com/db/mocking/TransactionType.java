package com.db.mocking;

public enum TransactionType {

    DEBIT, CREDIT

}
