package com.db.mocking;

/**
 * @author me
 * Implemented by me
 */
public interface IBank {

    public String withdraw(int amt, String id);
    public String deposit(int amt, String id);

}
