package com.db.mocking;


/**
 * @author Someone else
 * Not implemented
 */
public interface IBankService {

    public String transaction(int amt, String id, TransactionType type);

}
