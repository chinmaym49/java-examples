package com.db.application;

class ApplicationImpl implements IApplication{

    private double bal;

    ApplicationImpl() {
        bal=100.0;
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public void deposit(double amt) {
        bal+=amt;
        System.out.println("Current balance after depositing: "+bal);
    }

    @Override
    public void withdraw(double amt) {
        bal-=amt;
        System.out.println("Current balance after withdrawing: "+bal);
    }
}
