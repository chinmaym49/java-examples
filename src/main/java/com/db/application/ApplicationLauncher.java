package com.db.application;

public class ApplicationLauncher {

    public static void main(String argv[]) {

         IApplication app = ApplicationFactory.getApp();
         app.deposit(25);
         app.withdraw(50);

    }
}
