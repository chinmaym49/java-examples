package com.db.application;

class ApplicationFactory {

    private static IApplication app = new ApplicationImpl();
    private static ApplicationFactory factory = new ApplicationFactory();
    private static LoggerProxy proxy = new LoggerProxy(app);

    /**
     * Singleton
     */
    private ApplicationFactory(){}

    /**
     * Factory method
     * @return IApplication
     */
    public static IApplication getApp() {
        return proxy;
    }

    public static ApplicationFactory getInstance() {
        return factory;
    }

}
