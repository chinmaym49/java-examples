package com.db.application;

/**
 * Basically, no need to put the logging code in ApplicationImpl
 */
class LoggerProxy implements IApplication {

    private IApplication app;

    public LoggerProxy(IApplication app) {
        this.app=app;
    }

    @Override
    public String getVersion() {
        System.out.println("Logging stuff");
        return app.getVersion();
    }

    @Override
    public void deposit(double amt) {
        System.out.println("Logging stuff");
        app.deposit(amt);
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Logging stuff");
        app.withdraw(amt);
    }
}
