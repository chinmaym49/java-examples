package com.db.application;

public interface IApplication {

    /**
     * Returns version
     * @return String
     */
    String getVersion();

    /**
     * Depositing
     * @param amt
     */
    void deposit(double amt);

    /**
     * Withdrawing
     * @param amt
     */
    void withdraw(double amt);

}
