package com.db.collections;

import java.util.*;

public class CollecExample {

    public static void main(String[] args) {

        useList();
        useSet();
        useMap();

    }

    private static void useList() {

        List<String> l = new ArrayList<>();
        l.add("123");
        l.add("abc");
        System.out.println(l.size());
        System.out.println(l);

    }

    private static void useSet() {

        Set<Integer> s = new HashSet<>();
        Integer x=1;
        s.add(x);
        s.add(1);
        s.add(2);
        s.add(2);
        System.out.println(s);

    }

    private static void useMap() {

        Map<Integer,String> m = new HashMap<>();
        for(int i=0; i<5; i++)
            m.put(i, String.valueOf(i+10));

        System.out.println(m.get(4));
        System.out.println(m.getOrDefault(10, "-"));

    }
}
