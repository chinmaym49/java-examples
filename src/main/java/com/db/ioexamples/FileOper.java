package com.db.ioexamples;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileOper {

    /**
     * Readers/Writers - Textual(char, ascii etc)
     * InputStream/OutputStream - Non-textual(bytes, img etc)
     * NativeStreams - Very fast
     *
     * @param args
     */
    public static void main(String[] args) {

        readFileInputStream();
        readFileReader();
        readFileStreams();

    }

    private static void readFileInputStream() {

        InputStream is = null;
        try {
            is = new FileInputStream("E:\\Programming\\Java\\javalearnables\\tmp.txt");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            System.out.println(new String(buffer));
        } catch(Exception e) {
            System.out.println("An error occured!");
        } finally {
            try {
                is.close();
            } catch (Exception e) {}
        }
    }

    private static void readFileReader() {

        String data = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("E:\\Programming\\Java\\javalearnables\\temp.txt"));
            while((data= reader.readLine()) != null)
                System.out.println(data);
        } catch(Exception e) {
            System.out.println("An error occured!");
        } finally {
            try {
                reader.close();
            } catch(Exception e) {}
        }
    }

    private static void readFileStreams() {

        try {
            List<String> data =Files.readAllLines(Paths.get("E:\\Programming\\Java\\javalearnables\\temp.txt"));
            System.out.println(data);
        } catch(Exception e) {
            System.out.println("An error occured!");
        }

    }
}
